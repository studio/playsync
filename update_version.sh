#!/bin/bash

VERSION="${1/v}"

if [ -z "$VERSION" ]; then
    echo "Usage: $0 new-version" >&2
    exit 1
fi

BL_INFO_VER=$(echo "$VERSION" | sed 's/\./, /g')

sed "s/version = \"[^\"]*\"/version = \"$VERSION\"/" -i playsync-addon/pyproject.toml
sed "s/\"version\": ([^)]*)/\"version\": ($BL_INFO_VER)/" -i playsync-addon/playsync/__init__.py

git diff
echo
echo "Don't forget to commit and tag:"
echo git commit -m \'Bumped version to $VERSION\' playsync-addon/pyproject.toml playsync-addon/playsync/__init__.py
echo git tag -a v$VERSION -m \'Tagged version $VERSION\'
