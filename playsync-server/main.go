package main

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"

	"gitlab.com/blender/playsync/playsync-server/playsync"
	"gitlab.com/blender/playsync/playsync-server/playsync/config"
	"gitlab.com/blender/playsync/playsync-server/playsync/filewaiter"
	"gitlab.com/blender/playsync/playsync-server/playsync/httpserver"
)

const applicationName = "PlaySync"

var applicationVersion = "set-during-build"

func main() {
	parseCliArgs()
	configLogging()
	log.WithField("version", applicationVersion).Infof("starting %s", applicationName)

	defer func() {
		// If there was a panic, make sure we log it before quitting.
		if r := recover(); r != nil {
			log.Panic(r)
		}
	}()

	shutdownComplete = make(chan struct{})

	// Handle Ctrl+C
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		for signum := range c {
			// Run the shutdown sequence in a goroutine, so that multiple Ctrl+C presses can be handled in parallel.
			go shutdown(signum)
		}
	}()

	var err error
	conf, err = config.GetConf()
	if err != nil {
		logrus.WithError(err).Fatal("unable to load configuration")
	}

	go func() {
		// Wait for the TLS certificate files to appear via the ACME protocol.
		if err := filewaiter.WaitFor(conf.TLSCert); err != nil {
			logrus.WithError(err).Fatal("TLS certificate does not exist, and ACME protocol is not downloading it")
		}
		// Wait one more second for the file to be written.
		<-time.After(time.Second)

		server = playsync.NewServer(conf)
		go server.Start()
	}()

	httpServer = httpserver.New(conf)
	httpServer.ListenAndServe()

	<-shutdownComplete
}
