package main

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

import (
	"context"
	"flag"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/blender/playsync/playsync-server/playsync"
	"gitlab.com/blender/playsync/playsync-server/playsync/config"
	"gitlab.com/blender/playsync/playsync-server/playsync/httpserver"
)

var (
	conf       config.Conf
	server     playsync.Server
	httpServer httpserver.Server
)

var shutdownComplete chan struct{}

func shutdown(signum os.Signal) {
	shutdownDone := make(chan bool)

	go func() {
		log.WithField("signal", signum).Info("Signal received, shutting down.")

		if server != nil {
			server.Shutdown()
		}

		if httpServer != nil {
			log.Info("Shutting down HTTP server")
			shutdownCtx, shutdownCtxCancel := context.WithTimeout(context.Background(), httpserver.ReadTimeout+1*time.Second)
			defer shutdownCtxCancel()

			// the Shutdown() function seems to hang sometime, even though the
			// main goroutine continues execution after ListenAndServe().
			go httpServer.Shutdown(shutdownCtx)
			<-httpServer.Done()
		} else {
			log.Warning("HTTP server was not even started yet")
		}

		shutdownDone <- true
	}()

	// Force shutdown after waiting for too long
	select {
	case <-shutdownDone:
		break
	case <-time.After(5 * time.Second):
		log.Error("Shutdown forced, stopping process.")
		os.Exit(-2)
	}

	log.Warning("Shutdown complete, stopping process.")
	close(shutdownComplete)
}

// Commandline argument values.
var cliArgs struct {
	// Influence logging:
	verbose bool
	quiet   bool
	debug   bool
	jsonLog bool

	// Options that run a certain operation, then exit the process:
	version bool

	// Configuration:
	hostname string
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.verbose, "verbose", false, "Ignored as this is now the default")
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging")
	flag.BoolVar(&cliArgs.jsonLog, "json", false, "Log in JSON format")
	flag.BoolVar(&cliArgs.version, "version", false, "Show the version of Flamenco Manager")
	flag.StringVar(&cliArgs.hostname, "hostname", "localhost", "Hostname for TLS certificates")
	flag.Parse()
}

func configLogging() {
	if cliArgs.jsonLog {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{
			FullTimestamp: true,
		})
	}

	// Only log the warning severity or above.
	level := log.InfoLevel
	if cliArgs.debug {
		level = log.DebugLevel
	} else if cliArgs.quiet {
		level = log.WarnLevel
	}
	log.SetLevel(level)
}
