package playsync

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

import (
	"bufio"
	"encoding/json"
	"errors"
	"net"
	"time"

	"github.com/sirupsen/logrus"
)

// ErrUnexpectedClientMessage is returned when a client sends a message type we don't expect.
var ErrUnexpectedClientMessage = errors.New("unexpected message type received from client")

type clientConnection struct {
	conn     net.Conn
	buffer   *bufio.ReadWriter
	logger   logrus.FieldLogger
	nickname string
}

func newClientConnection(conn net.Conn) *clientConnection {
	client := &clientConnection{
		conn,
		bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn)),
		logrus.WithField("remote", conn.RemoteAddr()),
		"",
	}
	return client
}

func (c *clientConnection) sendGreeting() error {
	return c.sendMessage(NewEmptyMessage(S2CGreeting))
}

func (c *clientConnection) confirmJoin() error {
	return c.sendMessage(NewEmptyMessage(S2CJoinRoomConfirm))
}

func (c *clientConnection) waitToJoin() (roomID string, err error) {
	message, err := c.receiveMessage()
	if err != nil {
		return "", err
	}
	if message.Type != C2SJoinRoom {
		return "", ErrUnexpectedClientMessage
	}

	payload := JoinRoomPayload{}
	if err := json.Unmarshal(message.Payload, &payload); err != nil {
		return "", err
	}

	c.nickname = payload.Nickname
	c.logger = c.logger.WithField("nickname", payload.Nickname)
	return payload.RoomID, nil
}

func (c *clientConnection) joinedRoom(roomID string) {
	c.logger = c.logger.WithField("room", roomID)
}

func (c *clientConnection) extendTimeout() {
	c.conn.SetDeadline(time.Now().Add(clientTimeout))
}

func (c *clientConnection) sendMessage(message *Message) error {
	if err := message.SendTo(c.buffer); err != nil {
		return err
	}
	return c.buffer.Flush()
}

func (c *clientConnection) receiveMessage() (*Message, error) {
	return ReceiveMessageFrom(c.buffer)
}
