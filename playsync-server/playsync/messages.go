package playsync

import (
	"encoding/binary"
	"errors"
	"io"
)

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

const protocolVersion = 1

var endianness = binary.BigEndian

// ErrUnsupportedProtocol is returned when the server receives a message with a different protocl number.
var ErrUnsupportedProtocol = errors.New("client is using an unsupported protocol version")

// Type indicates the message type.
type Type uint8

// Message represents a network message, either received or to send.
//
// The protocol is pretty simple. All messages are sent in network byte order
// (big-endian). The message type indicates whether a message is
// client-to-server, server-to-client, or client-to-client. The C2C messages can
// only be sent once a room has been joined, and will be broadcast to all the
// other clients in the same room.
type Message struct {
	Version            uint8
	Type               Type
	PayloadLengthBytes uint32
	Payload            []byte
	// TODO(Sybren): add checksum/HMAC
}

// Bitmasks that determine the direction of messages:
const (
	MaskS2C  = 0x40
	MaskC2S  = 0x80
	MaskC2C  = 0xc0
	MaskBits = 0xc0
)

// Server to Client messages
const (
	S2CGreeting        = Type(MaskS2C | 0x01)
	S2CJoinRoomConfirm = Type(MaskS2C | 0x02)
)

// Client to Server messages
const (
	C2SJoinRoom = Type(MaskC2S | 0x01) // Expect a JoinRoomPayload as payload.
)

// Client to Client Broadcast messages
const (
	C2CVSEStrips = Type(MaskC2C | 0x01) // VSE strip updates
	C2CPlayState = Type(MaskC2C | 0x02) // Playhead + playing/stopped
)

func (t Type) mask() uint8 {
	return uint8(t) & MaskBits
}

// JoinRoomPayload is received from a client when they want to join a room.
type JoinRoomPayload struct {
	RoomID   string `json:"room_id"`
	Nickname string `json:"nickname"`
}

// NewEmptyMessage constructs a Message with the given type and no payload.
func NewEmptyMessage(messageType Type) *Message {
	message := &Message{
		protocolVersion,
		messageType,
		0,
		[]byte{},
	}
	return message
}

// SendTo sends the message to a Writer, for example a network socket.
func (m *Message) SendTo(writer io.Writer) error {
	if err := binary.Write(writer, endianness, m.Version); err != nil {
		return err
	}
	if err := binary.Write(writer, endianness, m.Type); err != nil {
		return err
	}
	if err := binary.Write(writer, endianness, m.PayloadLengthBytes); err != nil {
		return err
	}
	if _, err := writer.Write(m.Payload); err != nil {
		return err
	}
	return nil
}

// ReceiveMessageFrom constructs a message by reading it from a Reader, for example a network socket.
func ReceiveMessageFrom(reader io.Reader) (*Message, error) {
	var message Message
	var err error
	if err = binary.Read(reader, endianness, &message.Version); err != nil {
		return &message, err
	}
	if message.Version != protocolVersion {
		// Unable to read any further.
		return &message, ErrUnsupportedProtocol
	}

	if err = binary.Read(reader, endianness, &message.Type); err != nil {
		return &message, err
	}
	if err = binary.Read(reader, endianness, &message.PayloadLengthBytes); err != nil {
		return &message, err
	}
	if message.PayloadLengthBytes == 0 {
		return &message, nil
	}

	message.Payload = make([]byte, message.PayloadLengthBytes)
	if _, err = io.ReadFull(reader, message.Payload); err != nil {
		return &message, err
	}
	return &message, nil
}
