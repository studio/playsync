package httpserver

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

import (
	"context"
	"net/http"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/acme/autocert"

	playsyncConfig "gitlab.com/blender/playsync/playsync-server/playsync/config"
)

// Constants for the HTTP servers.
const (
	ReadHeaderTimeout = 15 * time.Second
	ReadTimeout       = 600 * time.Second
)

// Server acts as a http.Server
type Server interface {
	Shutdown(ctx context.Context)
	ListenAndServe() error
	Done() <-chan struct{}
}

// Combined has one or two HTTP servers.
type Combined struct {
	httpServer  *http.Server
	httpsServer *http.Server

	tlsKey  string
	tlsCert string

	expectShutdown   bool
	mutex            sync.Mutex
	shutdownComplete chan struct{} // closed when ListenAndServe stops serving.
}

// New returns a new HTTP server that can handle HTTP, HTTPS, and both for ACME.
func New(config playsyncConfig.Conf) Server {
	server := Combined{
		mutex:            sync.Mutex{},
		shutdownComplete: make(chan struct{}),
	}

	handler := http.FileServer(http.Dir("wwwroot"))

	switch {
	case config.ACMEDomainName != "":
		logrus.WithFields(logrus.Fields{
			"acme_cert_dir":      config.ACMECertDirectory,
			"acme_directory_url": autocert.DefaultACMEDirectory,
			"acme_domain_name":   config.ACMEDomainName,
			"listen_http":        config.ListenHTTP,
			"listen_https":       config.ListenHTTPS,
		}).Info("creating ACME/Let's Encrypt enabled server")
		mgr := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(config.ACMEDomainName),
			Cache:      autocert.DirCache(config.ACMECertDirectory),
		}

		server.httpServer = &http.Server{
			Addr:    config.ListenHTTP,
			Handler: mgr.HTTPHandler(nil),
		}

		server.httpsServer = &http.Server{
			Addr:              config.ListenHTTPS,
			Handler:           handler,
			ReadTimeout:       ReadTimeout,
			ReadHeaderTimeout: ReadHeaderTimeout,
			TLSConfig:         mgr.TLSConfig(),
		}

	case config.HasCustomTLS():
		logrus.WithFields(logrus.Fields{
			"tlscert":      config.TLSCert,
			"tlskey":       config.TLSKey,
			"listen_https": config.ListenHTTPS,
		}).Info("creating HTTPS-enabled server")
		server.httpsServer = &http.Server{
			Addr:              config.ListenHTTPS,
			Handler:           handler,
			ReadTimeout:       ReadTimeout,
			ReadHeaderTimeout: ReadHeaderTimeout,
		}
		server.tlsKey = config.TLSKey
		server.tlsCert = config.TLSCert

	default:
		logrus.Fatal("either custom TLS or ACME needs to be configured")
	}

	return &server
}

// Shutdown shuts down both HTTP and HTTPS server.
func (s *Combined) Shutdown(ctx context.Context) {
	s.mutex.Lock()
	s.expectShutdown = true
	s.mutex.Unlock()

	if s.httpServer != nil {
		s.httpServer.Shutdown(ctx)
	}
	if s.httpsServer != nil {
		s.httpsServer.Shutdown(ctx)
	}
}

// Done returns a channel that is closed when the server is done serving.
func (s *Combined) Done() <-chan struct{} {
	return s.shutdownComplete
}

func (s *Combined) mustBeFresh() {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.expectShutdown {
		panic("this HTTP server was already shut down, unable to restart")
	}
}

// ListenAndServe listens on both HTTP and HTTPS servers.
func (s *Combined) ListenAndServe() error {
	s.mustBeFresh()
	defer close(s.shutdownComplete)

	var httpError, httpsError error
	wg := sync.WaitGroup{}

	if s.httpServer != nil {
		wg.Add(1)
		go func() {
			logger := logrus.WithField("listen", s.httpServer.Addr)
			logger.Debug("starting HTTP server")
			err := s.httpServer.ListenAndServe()

			s.mutex.Lock()
			defer s.mutex.Unlock()

			if !s.expectShutdown {
				logger.WithError(httpError).Error("HTTP server unexpectedly stopped")
				httpError = err
			}
			wg.Done()
		}()
	}

	if s.httpsServer != nil {
		wg.Add(1)
		go func() {
			logger := logrus.WithField("listen_https", s.httpsServer.Addr)
			logger.Debug("starting HTTPS server")
			err := s.httpsServer.ListenAndServeTLS(s.tlsCert, s.tlsKey)

			s.mutex.Lock()
			defer s.mutex.Unlock()

			if !s.expectShutdown {
				logger.WithError(err).Error("HTTPS server unexpectedly stopped")
				httpsError = err
			}
			wg.Done()
		}()
	}

	wg.Wait()

	// We can only return one error.
	if httpsError != nil {
		return httpsError
	}
	return httpError
}
