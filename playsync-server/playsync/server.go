package playsync

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

import (
	"crypto/tls"
	"io"
	"net"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/blender/playsync/playsync-server/playsync/config"
)

type chatroom map[*clientConnection]bool

type playSyncServer struct {
	shutdownChan chan struct{}
	wg           sync.WaitGroup

	conf      config.Conf
	chatrooms map[string]chatroom
	mutex     sync.Mutex
}

// NewServer creates a new PlaySync server.
func NewServer(conf config.Conf) Server {
	serverInstance := &playSyncServer{
		make(chan struct{}),
		sync.WaitGroup{},
		conf,
		map[string]chatroom{},
		sync.Mutex{},
	}

	return serverInstance
}

func (s *playSyncServer) Start() {
	logrus.Info("PlaySync server starting")

	tlsConfig := s.loadTLS()
	logger := logrus.WithField("addr", s.conf.ListenPlaySync)
	listener, err := tls.Listen("tcp", s.conf.ListenPlaySync, tlsConfig)
	if err != nil {
		logger.WithError(err).Fatal("unable to open listening port")
	}
	logger.Info("listening")

	for {
		conn, err := listener.Accept()
		if err != nil {
			logger.WithError(err).Warning("unable to accept incoming connection")
			continue
		}
		go s.handleConnection(conn)
	}
}

func (s *playSyncServer) Shutdown() {
	logrus.Info("PlaySync server shutting down")
	close(s.shutdownChan)
	s.wg.Wait()
}

func (s *playSyncServer) loadTLS() *tls.Config {
	log := logrus.WithFields(logrus.Fields{
		"cert": s.conf.TLSCert,
		"key":  s.conf.TLSKey,
	})
	log.Debug("loading TLS certificate")
	cer, err := tls.LoadX509KeyPair(s.conf.TLSCert, s.conf.TLSKey)
	if err != nil {
		log.WithError(err).Fatal("unable to load TLS certificates")
	}
	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	return config
}

func (s *playSyncServer) addClientToRoom(roomID string, client *clientConnection) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	logger := client.logger.WithField("room", roomID)

	room, exists := s.chatrooms[roomID]
	if !exists {
		logger.Info("creating room")
		room = chatroom{}
	} else {
		logger.Info("joining existing room")
	}

	room[client] = true
	s.chatrooms[roomID] = room

	client.joinedRoom(roomID)
}

func (s *playSyncServer) removeClientFromRoom(roomID string, client *clientConnection) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.chatrooms[roomID], client)
}

func (s *playSyncServer) handleConnection(conn net.Conn) {
	defer conn.Close()

	logger := logrus.WithField("remote", conn.RemoteAddr())
	logger.Info("client connected")
	defer logger.Debug("client connection closed")

	client := newClientConnection(conn)
	if err := client.sendGreeting(); err != nil {
		logger.WithError(err).Warning("error sending greeting to client")
		return
	}

	client.extendTimeout()
	roomID, err := client.waitToJoin()
	if err != nil {
		return
	}

	s.addClientToRoom(roomID, client)
	defer s.removeClientFromRoom(roomID, client)
	client.confirmJoin()

	for {
		msg, err := client.receiveMessage()
		if err != nil {
			if err == io.EOF {
				client.logger.Info("client disconnected")
			} else {
				client.logger.WithError(err).Warning("error receiving message, disconnecting client")
			}
			break
		}
		client.extendTimeout()

		switch msg.Type.mask() {
		case MaskC2C:
			s.broadcastToRoom(roomID, msg, client)
		default:
			client.logger.WithField("msgtype", msg.Type).Warning("unexpected message received, ignoring")
		}
	}
}

func (s *playSyncServer) broadcastToRoom(roomID string, msg *Message, fromClient *clientConnection) {
	// TODO(Sybren): lock the room, so that only one message gets broadcast at a time.
	for client := range s.chatrooms[roomID] {
		if client == fromClient {
			continue
		}

		// TODO(Sybren): remove clients that don't receive broadcast any more.
		client.sendMessage(msg)
	}
}
