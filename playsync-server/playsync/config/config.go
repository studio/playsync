package config

import (
	"io/ioutil"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

const (
	configFilename = "playsync-server.yaml"
)

var (
	// The default configuration, use DefaultConfig() to obtain a copy.
	defaultConfig = Conf{
		ListenPlaySync: ":8888",
		ListenHTTP:     ":8080",
		ListenHTTPS:    ":8433",

		ACMECertDirectory: "tlscerts",
	}
)

// Conf contains the PlaySync server configuration.
type Conf struct {
	ListenPlaySync string `yaml:"listen_playsync"`
	ListenHTTP     string `yaml:"listen_http"`
	ListenHTTPS    string `yaml:"listen_https"`

	// TLS certificate management.
	ACMEDomainName    string `yaml:"acme_domain_name"` // for the ACME Let's Encrypt client
	TLSKey            string `yaml:"tls_key"`
	TLSCert           string `yaml:"tls_cert"`
	ACMECertDirectory string `yaml:"tls_directory"`
}

// GetConf parses flamenco-manager.yaml and returns its contents as a Conf object.
func GetConf() (Conf, error) {
	return LoadConf(configFilename)
}

// DefaultConfig returns a copy of the default configuration.
func DefaultConfig() Conf {
	return defaultConfig
}

// LoadConf parses the given file and returns its contents as a Conf object.
func LoadConf(filename string) (Conf, error) {
	logger := logrus.WithField("config", filename)
	logger.Info("loading configuration")

	c := DefaultConfig()

	yamlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		return c, err
	}

	if err := yaml.Unmarshal(yamlFile, &c); err != nil {
		logger.WithError(err).Fatal("unable to parse YAML")
	}

	c.updateTLSConfig()

	return c, nil
}

// HasCustomTLS returns true if both the TLS certificate and key files are configured.
func (c *Conf) HasCustomTLS() bool {
	return c.TLSCert != "" && c.TLSKey != ""
}

func (c *Conf) updateTLSConfig() {
	if c.TLSCert == "" {
		c.TLSCert = filepath.Join(c.ACMECertDirectory, c.ACMEDomainName)
	}
	if c.TLSKey == "" {
		c.TLSKey = filepath.Join(c.ACMECertDirectory, c.ACMEDomainName)
	}
}
