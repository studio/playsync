package filewaiter

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Blender Foundation. */

const (
	timeoutWait     = 10 * time.Minute
	fileCheckPeriod = 2 * time.Second
)

// WaitFor waits for a file to appear on the filesystem, then returns.
func WaitFor(filename string) error {
	logger := logrus.WithField("filename", filename)
	if exists, _ := fileExists(filename); exists {
		logger.Debug("file exists, not waiting")
		return nil
	}

	deadlineTimer := time.NewTimer(10 * time.Minute)
	defer deadlineTimer.Stop()

	logger.Debug("waiting for file to appear")

	for {
		select {
		case <-deadlineTimer.C:
			logger.Debug("file did not appear")
			return os.ErrNotExist
		case <-time.After(fileCheckPeriod):
			exists, err := fileExists(filename)
			if exists || err != nil {
				return err
			}
		}
	}
}

func fileExists(filename string) (bool, error) {
	_, err := os.Stat(filename)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
