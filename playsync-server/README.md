# PlaySync Server

This server is only reachable via TLS-encrypted sockets. A TLS certificate can be automatically obtained from Let's Encrypt by configuring the domain name in `playsync-server.yaml`.

## Configuration

Configuration is read from the file `playsync-server.yaml`. Here is an example:

```
listen_playsync: ":8888"
listen_http: ":8080"
listen_https: ":8443"

acme_domain_name: playsync.example.com
```

It is recommended to have all TCP/IP port numbers > 1024 so that superuser permissions are not required, and to set up firewall rules to redirect ports 80 and 443 to respectively the configured HTTP and HTTPS ports.

## Self-signed TLS

To use a self-signed TLS certificate, set `acme_domain_name: ""` in the `playsync-server.yaml` configuration file, then run either of these commands:

With a new key:
```
openssl req -x509 -newkey rsa:4096 -keyout tlscerts/privkey.pem -out tlscerts/cert.pem -days 365 -nodes
```

With an existing key:
```
openssl req -x509 -key tlscerts/privkey.pem -out tlscerts/cert.pem -days 365 -nodes
```

The server certificate must be placed inside the add-on, in `playsync/server_cert.pem`. This is only necessary for self-signed certificates.
