#!/usr/bin/env python

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.


import dataclasses
import socket
import json
import enum
import ssl
import struct
from typing import Any


class MessageType(enum.Enum):
    """Message types, as defined by the server in messages.go."""

    # Bitmasks that determine the direction of messages:
    MaskS2C = 0x40
    MaskC2S = 0x80
    MaskC2C = 0xC0

    # Server to Client messages:
    S2CGreeting = MaskS2C | 0x01
    S2CJoinRoomConfirm = MaskS2C | 0x02

    # Client to Server messages:
    C2SJoinRoom = MaskC2S | 0x01

    # Broadcast messages:
    # VSE strip updates, payload = data_differ.DataDiff
    C2CVSEStrips = MaskC2C | 0x01
    # Playhead + playing/stopped, payload = data_playstate.PlayState
    C2CPlayState = MaskC2C | 0x02


PROTOCOL_VERSION = 1


@dataclasses.dataclass
class Message:
    version: int
    message_type: MessageType
    length: int
    body: bytes

    @classmethod
    def receive_from(cls, from_socket: socket.socket) -> "Message":
        message_version = int.from_bytes(sock.recv(1), "big")
        if message_version != PROTOCOL_VERSION:
            raise RuntimeError(
                f"received message with protocol {message_version}, expected {PROTOCOL_VERSION}"
            )

        message_type = int.from_bytes(sock.recv(1), "big")
        message_length = int.from_bytes(sock.recv(4), "big")
        if message_length > 0:
            message_body = sock.recv(message_length)
        else:
            message_body = b""

        return cls(
            message_version, MessageType(message_type), message_length, message_body
        )

    @classmethod
    def empty(cls, message_type: MessageType) -> "Message":
        return Message(
            version=PROTOCOL_VERSION, message_type=message_type, length=0, body=b"",
        )

    @classmethod
    def with_payload(cls, message_type: MessageType, payload: Any) -> "Message":
        payload_str = json.dumps(payload)
        payload_bytes = payload_str.encode("utf8")
        return Message(
            version=PROTOCOL_VERSION,
            message_type=message_type,
            length=len(payload_bytes),
            body=payload_bytes,
        )

    def send_to(self, to_socket: socket.socket) -> None:
        buf = struct.pack("!BBI", self.version, self.message_type.value, self.length)
        to_socket.sendall(buf)
        if self.length > 0:
            to_socket.sendall(self.body)


message_buffer = b""
hostname = 'playsync.blender.cloud'
insecure_sock = socket.socket(socket.AF_INET)

# Wrap socket with TLS.
tls_context = ssl.create_default_context()
sock = tls_context.wrap_socket(insecure_sock, server_hostname=hostname)
print(f"socket TLS-wrapped")

# Connect.
sock.settimeout(10)
sock.connect((hostname, 8888))
print(f"Connected to {sock.getpeername()}")

msg = Message.receive_from(sock)
if msg.message_type != MessageType.S2CGreeting:
    print(f"Unexpected message received: {msg}")


room_id = "public"
print(f"Server greeting received, going to join room {room_id!r}")
msg = Message.with_payload(MessageType.C2SJoinRoom, {"room_id": room_id})
msg.send_to(sock)

msg = Message.receive_from(sock)
if msg.message_type != MessageType.S2CJoinRoomConfirm:
    print(f"Unexpected message received: {msg}")

print(f"Room join was confirmed!")

while True:
    msg = Message.receive_from(sock)
    print(msg)

sock.shutdown(socket.SHUT_RDWR)
sock.close()
