#!/usr/bin/env -S poetry run python

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import subprocess
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED


def main() -> None:
    subprocess.run(["poetry", "build", "--format", "wheel"], check=True, timeout=10.0)

    proc = subprocess.run(
        ["poetry", "version"], capture_output=True, check=True, timeout=10.0
    )
    addon_version = proc.stdout.decode().strip().split(" ")[-1]

    wheel_path = Path("dist") / f"PlaySync-{addon_version}-py3-none-any.whl"
    zip_path = Path("dist") / f"playsync-addon-{addon_version}.zip"
    with ZipFile(wheel_path) as wheelfile:
        with ZipFile(
            zip_path, "w", compression=ZIP_DEFLATED, compresslevel=9
        ) as zipfile:
            for filename in wheelfile.namelist():
                if ".dist-info" in filename:
                    continue
                zipfile.writestr(filename, wheelfile.read(filename))

    wheel_path.unlink()
    print(f"ZIP built in {zip_path}")
    print("deploy with:")
    print(f"scp {zip_path} playsync@playsync.blender.cloud:wwwroot/addon/")


if __name__ == "__main__":
    main()

