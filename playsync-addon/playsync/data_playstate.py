# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import dataclasses
import json
import logging
import time
from typing import Any, Dict, Optional

import bpy

from .message_payloads import PlayState

logger = logging.getLogger(__name__)

playstate_lock_release_sec = 1.0
playstate_send_period_sec = 0.25


class PlayStateDiffer:
    def __init__(self) -> None:
        self._last_sent_state: Optional[PlayState] = None
        self._frame_current = 0

    def forget_snapshot(self, context: bpy.types.Context) -> None:
        self._last_sent_state = self._current_state(context)

    def diff_and_snapshot(self, context: bpy.types.Context) -> Optional[PlayState]:
        """Return the PlayState to broadcast to others in the room.

        Returns None if nothing changes since the last call.
        """

        current_state = self._current_state(context)
        if current_state == self._last_sent_state:
            return None

        diff = self._compute_diff(current_state)
        self._last_sent_state = current_state
        return diff

    def apply(self, context: bpy.types.Context, state: PlayState) -> None:
        """Apply a PlayState as created by diff_and_snapshot()."""
        # TODO(Sybren): pass the context to the constructor so that it can be
        # used here instead of the global context.
        context = bpy.context

        current_state = self._current_state(context)

        # Prevent infinite loops
        self._last_sent_state = current_state

        if state == current_state:
            return

        if current_state.playing and state.playing:
            # While playing, don't sync the exact frame number.
            return

        logger.debug(
            "applying play state: frame %s -> %s  playing %s -> %s",
            current_state.frame_current,
            state.frame_current,
            current_state.playing,
            state.playing,
        )

        # Stopping is done before setting the current frame, so that all clients have the same current frame.
        apply_state_playing = state.playing is not None

        if apply_state_playing and current_state.playing and not state.playing:
            bpy.ops.screen.animation_cancel(restore_frame=False)

        if state.frame_current is not None:
            context.scene.frame_set(state.frame_current)

        # Starting is done after setting the frame, so that all clients start playback from the same frame.
        if apply_state_playing and not current_state.playing and state.playing:
            bpy.ops.screen.animation_play()

    def _current_state(self, context: bpy.types.Context) -> PlayState:
        screen = context.screen
        return PlayState(
            playing=screen.is_animation_playing and not screen.is_scrubbing,
            frame_current=context.scene.frame_current,
        )

    def _compute_diff(self, state_to: PlayState) -> Optional[PlayState]:
        if self._last_sent_state is None:
            return state_to

        if self._last_sent_state.playing and state_to.playing:
            # If playing, and we already sent an 'is playing' state, don't bother sending anything.
            return None

        playing = None
        if self._last_sent_state.playing != state_to.playing:
            playing = state_to.playing

        frame_current = None
        if self._last_sent_state.frame_current != state_to.frame_current:
            frame_current = state_to.frame_current

        return PlayState(playing, frame_current)


class TimeManager:
    def __init__(self) -> None:
        self._differ_playstate = PlayStateDiffer()
        self._last_received_playstate_time = 0.0
        self._last_constructed_playstate_time = 0.0

    def handle_received_state(
        self, context: bpy.types.Context, state: PlayState
    ) -> None:
        self._last_received_playstate_time = time.monotonic()
        self._differ_playstate.apply(context, state)

    def construct_state_to_send(
        self, context: bpy.types.Context
    ) -> Optional[PlayState]:
        now = time.monotonic()
        sec_since_last_received = now - self._last_received_playstate_time
        if sec_since_last_received < playstate_lock_release_sec:
            # Refuse to send any time update to the room when someone else is
            # already sending time updates.
            self._differ_playstate.forget_snapshot(context)
            return None

        sec_since_last_constructed = now - self._last_constructed_playstate_time
        if sec_since_last_constructed < playstate_send_period_sec:
            # Refuse to send time updates too quickly after each other.
            return None

        self._last_constructed_playstate_time = now
        return self._differ_playstate.diff_and_snapshot(context)
