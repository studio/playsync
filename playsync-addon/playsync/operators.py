# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import logging
from typing import Optional, Set

import bpy

from . import network, preferences

logger = logging.getLogger(__name__)


class PLAYSYNC_OT_connector(bpy.types.Operator):
    bl_idname = "playsync.connector"
    bl_label = "PlaySync Connect"
    bl_description = (
        "PlaySync client, can connect to a server and sync the VSE with other clients"
    )

    timer = None

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return context.scene and context.scene.sequence_editor is not None

    def invoke(self, context: bpy.types.Context, event: bpy.types.Event) -> Set[str]:

        prefs = preferences.get(context)
        if not prefs or not prefs.is_configured():
            self.report({"ERROR"}, "Configure the PlaySync add-on first")
            return {"CANCELLED"}

        existing_client = network.PlaySyncClient.get()
        if existing_client:
            self.client = existing_client
            self.report({"INFO"}, "PlaySync connection retained")
        else:
            self.client = network.PlaySyncClient(context)
            self.client.connect()

        wm = context.window_manager
        wm.modal_handler_add(self)
        self.timer = wm.event_timer_add(0.1, window=context.window)

        self.report({"INFO"}, "PlaySync connection established")
        return {"RUNNING_MODAL"}

    def quit(self, context: bpy.types.Context) -> None:
        context.window_manager.event_timer_remove(self.timer)

        if self.client:
            self.client.shutdown()

        self.report({"INFO"}, "Shut down PlaySync connection")

    def modal(self, context: bpy.types.Context, event: bpy.types.Event) -> Set[str]:
        if event.type != "TIMER":
            return {"PASS_THROUGH"}

        if not self.client:
            self.quit(context)
            return {"FINISHED"}

        self.client.tick(context)
        return {"PASS_THROUGH"}


class PLAYSYNC_OT_disconnect(bpy.types.Operator):
    bl_idname = "playsync.disconnect"
    bl_label = "PlaySync Disconnect"
    bl_description = "Disconnect the current PlaySync client"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return bool(network.PlaySyncClient.get())

    def execute(self, context: bpy.types.Context) -> Set[str]:
        client = network.PlaySyncClient.get()
        if not client:
            return {"FINISHED"}

        client.shutdown()
        return {"FINISHED"}


class PLAYSYNC_OT_preferences(bpy.types.Operator):
    bl_idname = "playsync.preferences"
    bl_label = "PlaySync Preferences"
    bl_description = "Show the PlaySync preferences"

    def execute(self, context: bpy.types.Context) -> Set[str]:
        context.window_manager.addon_search = "playsync"
        context.preferences.active_section = "ADDONS"
        bpy.ops.screen.userpref_show("INVOKE_DEFAULT")
        return {"FINISHED"}


classes = (
    PLAYSYNC_OT_connector,
    PLAYSYNC_OT_disconnect,
    PLAYSYNC_OT_preferences,
)
_register, _unregister = bpy.utils.register_classes_factory(classes)


def register() -> None:
    logger.debug("registering")
    _register()


def unregister() -> None:
    logger.debug("unregistering")
    _unregister()
