# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import logging

import bpy

from . import network

logger = logging.getLogger(__name__)


@bpy.app.handlers.persistent
def depsgraph_update_post(scene, depsgraph):
    playsync_client = network.PlaySyncClient.get()
    if playsync_client is None:
        return

    logger.debug("depsgraph_update_post()")
    playsync_client.queue_update()


@bpy.app.handlers.persistent
def load_post(_):
    logger.debug("load_post()")
    playsync_client = network.PlaySyncClient.get()
    if not playsync_client:
        return

    if playsync_client.is_syncing_current_blendfile(bpy.context):
        logger.info("current blendfile was reloaded, keeping PlaySync connection alive")
        bpy.ops.playsync.connector("INVOKE_DEFAULT")
        return

    logger.warning(
        "Shutting down PlaySync connection because user loaded a different blend file."
    )
    playsync_client.shutdown()


def register() -> None:
    logger.debug("registering")
    # bpy.app.handlers.frame_change_post.append(handler_send_frame_changed)
    bpy.app.handlers.depsgraph_update_post.append(depsgraph_update_post)
    # bpy.app.handlers.undo_pre.append(on_undo_redo_pre)
    # bpy.app.handlers.redo_pre.append(on_undo_redo_pre)
    # bpy.app.handlers.undo_post.append(on_undo_redo_post)
    # bpy.app.handlers.redo_post.append(on_undo_redo_post)
    bpy.app.handlers.load_post.append(load_post)


def unregister() -> None:
    logger.debug("unregistering")
    # bpy.app.handlers.frame_change_post.remove(handler_send_frame_changed)
    bpy.app.handlers.depsgraph_update_post.remove(depsgraph_update_post)
    # bpy.app.handlers.undo_pre.remove(on_undo_redo_pre)
    # bpy.app.handlers.redo_pre.remove(on_undo_redo_pre)
    # bpy.app.handlers.undo_post.remove(on_undo_redo_post)
    # bpy.app.handlers.redo_post.remove(on_undo_redo_post)
    bpy.app.handlers.load_post.remove(load_post)
