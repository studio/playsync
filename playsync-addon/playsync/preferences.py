# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import logging
from typing import Optional

import bpy

logger = logging.getLogger(__name__)


class Preferences(bpy.types.AddonPreferences):
    bl_idname = "playsync"

    nickname: bpy.props.StringProperty(  # type: ignore
        name="Nickname", description="Your Name",
    )
    room_id: bpy.props.StringProperty(  # type: ignore
        name="Room ID",
        description="Name of the 'sync room' you want to join",
        default="public",
    )

    server_host: bpy.props.StringProperty(  # type: ignore
        name="Server",
        description="Host name or IP address of the PlaySync server",
        default="playsync.blender.cloud",
    )
    server_port: bpy.props.IntProperty(  # type: ignore
        name="Server Port",
        description="Port number of the PlaySync server",
        default=8888,
    )

    def is_configured(self) -> bool:
        return bool(
            self.nickname and self.room_id and self.server_host and self.server_port
        )

    def draw(self, context):
        layout = self.layout

        layout.prop(self, "nickname")
        layout.prop(self, "room_id")

        col = layout.column(align=True)
        col.label(text="Advanced:")
        col.prop(self, "server_host")
        col.prop(self, "server_port")


def get(context: Optional[bpy.types.Context]) -> Optional[Preferences]:
    """Return the add-on's preferences, if there are any."""

    if context is None:
        context = bpy.context

    try:
        addon_info = context.preferences.addons["playsync"]
    except KeyError:
        return None

    assert isinstance(addon_info.preferences, Preferences)
    return addon_info.preferences


classes = (Preferences,)
_register, _unregister = bpy.utils.register_classes_factory(classes)


def register() -> None:
    logger.debug("registering")
    _register()


def unregister() -> None:
    logger.debug("unregistering")
    _unregister()
