# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import dataclasses
import enum
import json
import logging
import queue
import socket
import ssl
import time
import threading
from pathlib import Path
from typing import Any, Dict, Iterable, Optional, Tuple

import bpy
import mathutils
import requests  # for the TLS CA certificates

from . import data_differ, data_playstate, messages, message_payloads, preferences

logger = logging.getLogger(__name__)

timeout_connect_secs = 5
timeout_period_diffs_secs = 1.0
timeout_vse_updates_holdback_secs = 1.0
client_protocol_version = 1

MessagePayload = Dict[str, Any]


class JSONEncoder(json.JSONEncoder):
    _tuple_like_types = (
        mathutils.Color,
        mathutils.Vector,
        mathutils.Quaternion,
        bpy.types.bpy_prop_array,
        set,
    )

    def default(self, obj: Any) -> Any:
        if isinstance(obj, self._tuple_like_types):
            return tuple(obj)

        return super().default(obj)


class PlaySyncClient:
    _instance: Optional["PlaySyncClient"] = None

    @classmethod
    def get(cls) -> Optional["PlaySyncClient"]:
        return cls._instance

    def __init__(self, context: bpy.types.Context) -> None:
        assert context.scene is not None

        prefs = preferences.get(context)
        assert prefs is not None

        self._prefs_server_addr = (prefs.server_host, prefs.server_port)
        self._prefs_nickname = prefs.nickname
        self._prefs_room_id = prefs.room_id

        self._sock_insecure: Optional[socket.socket] = None
        self._sock: Optional[socket.socket] = None
        self._is_room_joined = False
        self._message_buffer: bytes = b""
        self._differ_vse = data_differ.VSEDiffer()
        self._time_manager = data_playstate.TimeManager()
        self._is_applying_diff = False
        self._vse_update_queued = False
        self._outgoing_queue: Optional[OutgoingMessageQueue] = None

        vse = context.scene.sequence_editor_create()
        self._differ_vse.take_snapshot(vse)

        # monotonic time when last VSE update was sent.
        self._last_sent_vse_update = 0.0
        self._blendfile = Path(context.blend_data.filepath).resolve()

    def __bool__(self) -> bool:
        """Return True iff the socket is connected."""
        return self._sock is not None

    def connect(self) -> None:
        """Connect to the server."""

        self._message_buffer = b""
        self._is_room_joined = False
        self._sock_insecure = socket.socket(socket.AF_INET)
        self._sock_insecure.settimeout(timeout_connect_secs)

        logger.info("connecting to %s:%d", *self._prefs_server_addr)
        tls_context = self._create_tls_context()
        self._sock = tls_context.wrap_socket(
            self._sock_insecure, server_hostname=self._prefs_server_addr[0]
        )

        self._sock.connect(self._prefs_server_addr)
        logger.info("Connected to %s", self._sock.getpeername())

        # After connecting, the socket should be non-blocking.
        self._sock.settimeout(0)

        self._outgoing_queue = OutgoingMessageQueue(self._sock)
        self._outgoing_queue.start()

        self.__class__._instance = self

    def shutdown(self) -> None:
        """Disconnect from server."""

        if self.__class__._instance == self:
            self.__class__._instance = None

        if self._outgoing_queue:
            self._outgoing_queue.shutdown()

        if self._sock is None:
            return

        try:
            self._sock.shutdown(socket.SHUT_RDWR)
        except OSError:
            pass
        self._sock.close()

        self._sock = None

    def is_syncing_current_blendfile(self, context: bpy.types.Context) -> bool:
        current_blendfile = Path(context.blend_data.filepath).resolve()
        return current_blendfile == self._blendfile

    def tick(self, context: bpy.types.Context) -> None:
        """Handle incoming & outgoing messages."""
        self._receive_messages(context)
        if self._is_room_joined:
            self._periodic_diff(context)

    def _create_tls_context(self) -> ssl.SSLContext:
        tls_context = ssl.create_default_context()
        capath = self._find_capath()
        if capath:
            tls_context.load_verify_locations(str(capath))
        return tls_context

    @staticmethod
    def _find_capath() -> Optional[Path]:
        addon_path = Path(__file__).parent
        server_cert_path = addon_path / "server_cert.pem"
        if not server_cert_path.exists():
            return Path(requests.certs.where())  # type: ignore
        if server_cert_path.stat().st_size == 0:
            return Path(requests.certs.where())  # type: ignore
        logger.info("loading TLS certificate from %s", server_cert_path.absolute())
        return server_cert_path

    def _receive_messages(self, context: bpy.types.Context) -> None:
        """Receive & handle incoming data."""
        assert self._sock is not None

        try:
            self._message_buffer += self._sock.recv(128)
        except (BlockingIOError, ssl.SSLWantReadError) as ex:
            return

        message, self._message_buffer = messages.Message.receive_from(
            self._message_buffer
        )
        if not message:
            return

        logger.debug("received: %r", message)
        self._handle_message(context, message)

    def _handle_message(
        self, context: bpy.types.Context, message: messages.Message
    ) -> None:
        handlers = {
            messages.MessageType.S2CGreeting: self._on_s2c_greeting,
            messages.MessageType.S2CJoinRoomConfirm: self._on_s2c_join_room_confirm,
            messages.MessageType.C2CVSEStrips: self._on_c2c_vse_strips,
            messages.MessageType.C2CPlayState: self._on_c2c_play_state,
        }
        try:
            handler = handlers[message.message_type]
        except KeyError:
            logger.warning("unsupported message type %s received", message.message_type)
            return

        handler(context, message)

    def _on_s2c_greeting(
        self, context: bpy.types.Context, message: messages.Message
    ) -> None:
        """After receiving a greeting from the server, join the room."""
        self._is_room_joined = False
        self._send_message(
            messages.MessageType.C2SJoinRoom,
            {"room_id": self._prefs_room_id, "nickname": self._prefs_nickname},
        )

    def _on_s2c_join_room_confirm(
        self, context: bpy.types.Context, message: messages.Message
    ) -> None:
        logger.info("room join confirmed by server")
        self._is_room_joined = True

    def _on_c2c_vse_strips(
        self, context: bpy.types.Context, message: messages.Message
    ) -> None:
        logger.info("VSE state data received: %s", message)
        diff = data_differ.DiffResult.from_json_bytes(message.payload)
        vse = context.scene.sequence_editor

        self._is_applying_diff = True
        try:
            self._differ_vse.apply_diff(vse, diff)
        finally:
            self._is_applying_diff = False

    def _on_c2c_play_state(
        self, context: bpy.types.Context, message: messages.Message
    ) -> None:
        playstate = message_payloads.PlayState.from_json_bytes(message.payload)
        logger.info("playhead state received: %s", playstate)
        self._time_manager.handle_received_state(context, playstate)

    def _periodic_diff(self, context: bpy.types.Context) -> None:
        """Periodically inspect the current state of the VSE.

        Not all changes trigger a depsgraph update, hence a timer is used as a
        second source of "update now" signals.
        """
        self._check_outgoing_queue_thread()

        # TODO(Sybren): refactor so that network data is queued properly.
        playstate = self._time_manager.construct_state_to_send(context)
        if playstate:
            self._send_playstate_message(playstate)

        if self._differ_vse.time_since_last_snapshot > timeout_period_diffs_secs:
            self.queue_update()

        self._maybe_send_vse_update(context.scene.sequence_editor)

    def _send_playstate_message(self, playstate: data_playstate.PlayState) -> None:
        payload = playstate.asdict()
        logger.debug("sending play state %s", payload)
        self._send_message(messages.MessageType.C2CPlayState, payload)

    def queue_update(self) -> None:
        self._vse_update_queued = True

    def _maybe_send_vse_update(self, vse: bpy.types.SequenceEditor) -> None:
        if self._is_applying_diff or not self._vse_update_queued:
            return

        # Rate-limit the diffs, so that not every tiny drag is sent over the network.
        time_since_last_update = time.monotonic() - self._last_sent_vse_update
        if time_since_last_update < timeout_vse_updates_holdback_secs:
            return

        self._vse_update_queued = False

        diff = self._differ_vse.diff_and_snapshot(vse)
        if not diff:
            return

        logger.debug("sending diff to server: %s", diff)
        payload = diff.asdict()

        self._last_sent_vse_update = time.monotonic()
        self._send_message(messages.MessageType.C2CVSEStrips, payload)

    def _check_outgoing_queue_thread(self) -> None:
        if not self._outgoing_queue:
            logger.warning("outgoing queue disappeared, disconnecting client")
            self.shutdown()
            return

        if not self._outgoing_queue.is_alive():
            logger.warning("outgoing queue thread died, restarting it")
            assert self._sock is not None
            self._outgoing_queue = OutgoingMessageQueue(self._sock)
            self._outgoing_queue.start()
            return

        if not self._outgoing_queue.socket_ok:
            logger.warning("network connection lost, disconnecting client")
            self.shutdown()
            return

    def _send_message(
        self, msg_type: messages.MessageType, payload: MessagePayload
    ) -> None:
        assert self._outgoing_queue is not None
        self._outgoing_queue.queue(msg_type, payload)


class OutgoingMessageQueue(threading.Thread):
    def __init__(self, sock: socket.socket) -> None:
        super().__init__(daemon=True)
        self._queue: queue.Queue[Tuple[messages.MessageType, MessagePayload]]
        self._queue = queue.Queue(maxsize=4)
        self._do_shutdown = threading.Event()
        self._had_socket_read_error = threading.Event()
        self._sock = sock
        self._log = logger.getChild(self.__class__.__name__)

    def queue(self, msg_type: messages.MessageType, payload: MessagePayload) -> None:
        """Queues the message for sending in a separate thread."""
        self._queue.put_nowait((msg_type, payload))

    def shutdown(self) -> None:
        self._do_shutdown.set()
        self.join()

    def socket_ok(self) -> bool:
        return not self._had_socket_read_error.is_set()

    def run(self) -> None:
        try:
            self._thread_main()
        except Exception as ex:
            self._log.error("error in outgoing message queue: %s", ex)

    def _thread_main(self) -> None:
        while not self._do_shutdown.is_set():
            try:
                msg_type, payload = self._queue.get(block=True, timeout=0.5)
            except queue.Empty:
                continue

            self._send_message(msg_type, payload)

    def _send_message(
        self, msg_type: messages.MessageType, payload: MessagePayload
    ) -> None:
        assert self._sock is not None

        payload_str = json.dumps(payload, cls=JSONEncoder)
        payload_bytes = payload_str.encode("utf8")
        msg = messages.Message.with_payload(msg_type, payload_bytes)

        try:
            msg.send_to(self._sock)
        except BrokenPipeError as ex:
            logger.error(
                "connection to PlaySync server lost, shutting down client: %s", ex
            )
            self._had_socket_read_error.set()


def register() -> None:
    logger.debug("registering")


def unregister() -> None:
    logger.debug("unregistering")
