# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import logging

import bpy

from . import network, preferences


logger = logging.getLogger(__name__)

# The Screen.is_scrubbing property was introduced with Blender 2.90.
_blender_too_old = "is_scrubbing" not in bpy.types.Screen.bl_rna.properties


def draw_menu(self: bpy.types.TOPBAR_MT_app, context: bpy.types.Context) -> None:
    layout = self.layout
    layout.separator()

    if _blender_too_old:
        layout.label(text="PlaySync requires Blender 2.90 or newer")
        return

    addon_prefs = preferences.get(context)
    if not addon_prefs or not addon_prefs.is_configured():
        layout.operator(
            "playsync.preferences",
            text="PlaySync: configure the add-on first",
            icon="SETTINGS",
        )
        return

    client = network.PlaySyncClient.get()
    if client:
        layout.operator("playsync.disconnect", icon="X")
    else:
        layout.operator("playsync.connector", icon="UV_SYNC_SELECT")


def register() -> None:
    logger.debug("registering")
    bpy.types.TOPBAR_MT_app.append(draw_menu)


def unregister() -> None:
    logger.debug("unregistering")
    bpy.types.TOPBAR_MT_app.remove(draw_menu)
