# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

bl_info = {
    "name": "PlaySync",
    "author": "Sybren A. Stüvel <sybren@blender.org>",
    "description": "Collaborative VSE editing via networked Blenders.",
    "version": (0, 2, 0),
    "blender": (2, 90, 0),
    "location": "",
    "warning": "Experimental addon, can break your scenes",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Workflow",
}

import logging

# Support reloading
if "operators" not in locals():
    from . import (
        data_differ,
        data_playstate,
        hooks,
        messages,
        message_payloads,
        network,
        operators,
        preferences,
        user_interface,
    )
else:
    import importlib

    data_differ = importlib.reload(data_differ)
    data_playstate = importlib.reload(data_playstate)
    hooks = importlib.reload(hooks)
    messages = importlib.reload(messages)
    message_payloads = importlib.reload(message_payloads)
    network = importlib.reload(network)
    operators = importlib.reload(operators)
    preferences = importlib.reload(preferences)
    user_interface = importlib.reload(user_interface)

log = logging.getLogger(__name__)


def register() -> None:
    log.debug("registering")
    data_differ.register()
    hooks.register()
    operators.register()
    network.register()
    preferences.register()
    user_interface.register()


def unregister() -> None:
    log.debug("unregistering")
    user_interface.unregister()
    preferences.unregister()
    network.unregister()
    operators.unregister()
    hooks.unregister()
    data_differ.unregister()
