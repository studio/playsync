# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import dataclasses
import enum
import json
import socket
import struct
from typing import Optional, Tuple


class MessageType(enum.Enum):
    """Message types, as defined by the server in messages.go."""

    # Bitmasks that determine the direction of messages:
    MaskS2C = 0x40
    MaskC2S = 0x80
    MaskC2C = 0xC0

    # Server to Client messages:
    S2CGreeting = MaskS2C | 0x01
    S2CJoinRoomConfirm = MaskS2C | 0x02

    # Client to Server messages:
    C2SJoinRoom = MaskC2S | 0x01

    # Broadcast messages:
    # VSE strip updates, payload = data_differ.DataDiff
    C2CVSEStrips = MaskC2C | 0x01
    # Playhead + playing/stopped, payload = data_playstate.PlayState
    C2CPlayState = MaskC2C | 0x02


PROTOCOL_VERSION = 1
ENDIANNESS = "big"


class UnexpectedProtocolVersionError(ValueError):
    pass


@dataclasses.dataclass
class Message:
    version: int
    message_type: MessageType
    payload_length: int
    payload: bytes

    header_struct = struct.Struct("!BBI")

    @classmethod
    def _parse_header(cls, buffer: bytes) -> Tuple[int, MessageType, int]:
        msg_ver, msg_type, payload_len = cls.header_struct.unpack(
            buffer[: cls.header_struct.size]
        )
        if msg_ver != PROTOCOL_VERSION:
            raise UnexpectedProtocolVersionError(
                f"received message with protocol {msg_ver}, expected {PROTOCOL_VERSION}"
            )
        return msg_ver, msg_type, payload_len

    @classmethod
    def receive_from(cls, buffer: bytes) -> Tuple[Optional["Message"], bytes]:
        """Decode a message from the buffer.

        :return: the message (if any), and the remainder of the buffer.
        """

        if len(buffer) < cls.header_struct.size:
            # Not a complete message yet.
            return None, buffer

        msg_ver, msg_type, payload_length = cls._parse_header(buffer)

        msg_length = payload_length + cls.header_struct.size
        if len(buffer) < msg_length:
            # Not a complete message yet.
            return None, buffer

        if payload_length > 0:
            msg_payload = buffer[cls.header_struct.size : msg_length]
        else:
            msg_payload = b""

        msg = cls(msg_ver, MessageType(msg_type), payload_length, msg_payload)
        return msg, buffer[msg_length:]

    @classmethod
    def empty(cls, message_type: MessageType) -> "Message":
        return Message(
            version=PROTOCOL_VERSION,
            message_type=message_type,
            payload_length=0,
            payload=b"",
        )

    @classmethod
    def with_payload(cls, message_type: MessageType, payload: bytes) -> "Message":
        return Message(
            version=PROTOCOL_VERSION,
            message_type=message_type,
            payload_length=len(payload),
            payload=payload,
        )

    def send_to(self, to_socket: socket.socket) -> None:
        buf = self.header_struct.pack(
            self.version, self.message_type.value, self.payload_length
        )
        to_socket.sendall(buf)
        if self.payload_length > 0:
            to_socket.sendall(self.payload)
