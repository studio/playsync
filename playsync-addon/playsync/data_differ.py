# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import copy
import dataclasses
import json
import logging
import pprint
from pathlib import Path
import time
from typing import Any, Dict, Iterable, Optional, Set

import bpy

from .message_payloads import DiffResult, RNAData, StripSnapshot

logger = logging.getLogger(__name__)


class VSEDiffer:
    _exclude_names = {
        "rna_type",
        "is_evaluated",
        "original",
        "users",
        "tag",
        "is_library_indirect",
        "library",
        "override_library",
        "preview",
        "sequences",  # For now, nested sequences are not supported.
        "frame_final_end",  # Computed from start, offset, and duration.
        "frame_final_start",
        "animation_offset_start",
        "animation_offset_end",
        "fps",  # Opens the movie file to get the FPS, so it's potentially slow.
    }
    _tuple_like_types = (bpy.types.bpy_prop_array,)
    _frame_property_names = frozenset(
        (
            {
                propname
                for propname in bpy.types.Sequence.bl_rna.properties.keys()
                if propname.startswith("frame_")
            }
            - {"frame_duration"}
            - _exclude_names
        )
        | {"channel"}
    )

    def __init__(self) -> None:
        self._strip_creator = StripCreator()
        self.__strip_data: StripSnapshot = {}
        self.__active_strip_name = ""
        self._last_snapshot_time = 0.0  # monotonic clock timestamp of last snapshot.

    def pprint(self) -> None:
        pprint.pprint(self.__strip_data)

    def __getitem__(self, strip_name: str) -> RNAData:
        return self.__strip_data[strip_name]

    def take_snapshot(self, vse: bpy.types.SequenceEditor) -> None:
        """Take a snapshot of the current state."""
        snapshot = self._snapshot_strips(vse)
        self._set_snapshot(vse, snapshot)

    def diff_and_snapshot(self, vse: bpy.types.SequenceEditor) -> DiffResult:
        """Compute the diff from the last snapshot to the current state.

        Also takes a new snapshot, so that there cannot be any changes lost
        between diffing and snapshotting.
        """
        current_state = self._snapshot_strips(vse)

        removed_names = set(self.__strip_data.keys()) - set(current_state.keys())
        rna_diff = self._compute_diff(self.__strip_data, current_state)
        active_strip_diff = self._active_strip_diff(vse)

        self._set_snapshot(vse, current_state)

        return DiffResult(removed_names, rna_diff, active_strip_diff)

    def apply_diff(self, vse: bpy.types.SequenceEditor, diff: DiffResult) -> None:
        """Apply a diff like computed by diff_and_snapshot()."""

        self._remove_strips(vse, diff.removed_strip_names)
        self._set_properties_from_snapshot(vse, diff.rna_diff)
        self._set_active_strip(vse, diff.active_strip)

        # TODO(Sybren): instead of taking a new snapshot, update the snapshot
        # based on the incoming diff. That should prevent overwriting local
        # changes every time remote changes come in.
        self.take_snapshot(vse)

    @property
    def time_since_last_snapshot(self) -> float:
        """Time in seconds since last snapshot was taken."""
        return time.monotonic() - self._last_snapshot_time

    def _set_snapshot(
        self, vse: bpy.types.SequenceEditor, snapshot: StripSnapshot
    ) -> None:
        """Record the snapshot and remember the current time."""
        self.__strip_data = snapshot
        self.__active_strip_name = self._active_strip_name(vse)
        self._last_snapshot_time = time.monotonic()

    def _active_strip_name(self, vse: bpy.types.SequenceEditor) -> str:
        active_strip = vse.active_strip
        if active_strip is None:
            return ""
        assert isinstance(active_strip.name, str)
        return active_strip.name

    def _active_strip_diff(self, vse: bpy.types.SequenceEditor) -> Optional[str]:
        current_active = self._active_strip_name(vse)
        if self.__active_strip_name == current_active:
            return None
        return current_active

    def _remove_strips(
        self, vse: bpy.types.SequenceEditor, strip_names: Iterable[str]
    ) -> None:
        """Remove all strips with the given names."""

        for strip_name in strip_names:
            try:
                strip = vse.sequences[strip_name]
            except KeyError:
                # TODO(Sybren): this can also mean it was inside a meta-strip.
                continue
            vse.sequences.remove(strip)

    def _set_active_strip(
        self, vse: bpy.types.SequenceEditor, active_strip_name: Optional[str]
    ) -> None:
        logger.debug("setting active strip to %r", active_strip_name)
        if active_strip_name is None:
            # This means 'no change'
            return
        if not active_strip_name:
            vse.active_strip = None
            return
        try:
            strip = vse.sequences_all[active_strip_name]
        except KeyError:
            logger.warning("unable to find active strip %r", active_strip_name)
            return

        vse.active_strip = strip

    def _set_properties_from_snapshot(
        self, vse: bpy.types.SequenceEditor, snapshot: StripSnapshot
    ) -> None:
        """Update existing strips and create new ones.

        Does not delete non-referenced strips.
        """

        # Defer the creation of new strips until the rest of the snapshot has been dealt with.
        create_strip_names: Set[str] = set()

        for strip_name, strip_data in snapshot.items():
            try:
                strip = vse.sequences[strip_name]
            except KeyError:
                # TODO(Sybren): this can also mean it was inside a meta-strip.
                create_strip_names.add(strip_name)
                continue
            self._set_datablock_properties(strip, strip_data)

        for strip_name in create_strip_names:
            strip_data = snapshot[strip_name]
            strip = self._strip_creator.new(vse, strip_name, strip_data)
            if not strip:
                continue
            self._set_datablock_properties(strip, strip_data)

    def _set_datablock_properties(
        self, datablock: bpy.types.Sequence, properties: RNAData
    ) -> None:

        is_sequence = isinstance(datablock, bpy.types.Sequence)
        if is_sequence:
            self._set_sequence_properties(datablock, properties)
            return

        for prop_name, prop_value in properties.items():
            self._set_datablock_property(datablock, prop_name, prop_value)

    def _set_sequence_properties(
        self, strip: bpy.types.Sequence, properties: RNAData
    ) -> None:
        # The channel, frame start, and frame duration have to be set in the correct order, or things will get messy.
        prop_order = (
            "frame_offset_start",
            "frame_offset_end",
            "frame_still_start",
            "frame_still_end",
            "frame_start",
            "frame_final_start",
            "frame_final_duration",
            "channel",
        )

        remaining_prop_names = set(properties.keys())

        for prop_name in prop_order:
            try:
                remaining_prop_names.remove(prop_name)
            except KeyError:
                continue

            self._set_datablock_property(strip, prop_name, properties[prop_name])

        for prop_name in remaining_prop_names:
            if prop_name == "elements":
                # TODO(Sybren): properly update the sequence elements.
                continue
            self._set_datablock_property(strip, prop_name, properties[prop_name])

    def _set_datablock_property(
        self, datablock: bpy.types.Sequence, prop_name: str, prop_value: Any
    ) -> None:

        # Just try to set it, and ignore failures.
        # TODO(Sybren): be smarter about this, and also support nested objects.
        try:
            current_value = getattr(datablock, prop_name)
        except AttributeError:
            return

        if isinstance(prop_value, dict) and not isinstance(current_value, dict):
            # prop_value is an object that was serialised as dict.
            self._set_datablock_properties(current_value, prop_value)
            return

        try:
            setattr(datablock, prop_name, prop_value)
        except Exception as ex:
            str_ex = str(ex)
            if not str_ex.endswith("is read-only"):
                logger.warning(
                    "unable to set %r.%s = %r: %s",
                    datablock,
                    prop_name,
                    prop_value,
                    str_ex,
                )
            return

    def _snapshot_strips(self, vse: bpy.types.SequenceEditor) -> StripSnapshot:
        """Register the data as it is now, so we can compare later."""
        snapshot = {}
        for strip in vse.sequences:
            strip_data = self._construct_rna_data(strip)
            snapshot[strip.name] = strip_data
        return snapshot

    def _construct_rna_data(self, rna_datablock: Any) -> RNAData:
        # TODO(Sybren): turn this into a declarable type.
        assert hasattr(rna_datablock, "bl_rna")

        rna_data = {}
        property_names = (
            set(rna_datablock.bl_rna.properties.keys()) - self._exclude_names
        )
        for prop_name in property_names:
            value_reference = getattr(rna_datablock, prop_name)
            value_copy = self._copy_value(rna_datablock, prop_name, value_reference)
            rna_data[prop_name] = value_copy
        return rna_data

    def _copy_value(
        self, rna_datablock: Any, prop_name: str, value_reference: Any
    ) -> Any:
        value_copy: Any

        if isinstance(rna_datablock, bpy.types.Sequence) and prop_name == "elements":
            # Image sequence elements are serialised as just filenames.
            return [element.filename for element in value_reference]

        if hasattr(value_reference, "bl_rna"):
            return self._construct_rna_data(value_reference)

        if isinstance(value_reference, self._tuple_like_types):
            return tuple(value_reference)

        try:
            return copy.deepcopy(value_reference)
        except TypeError as ex:
            raise TypeError(
                f"unable to copy {rna_datablock!r}.{prop_name}: {ex}"
            ) from None

    @classmethod
    def _compute_diff(
        cls, from_state: StripSnapshot, to_state: StripSnapshot
    ) -> StripSnapshot:
        """Return minimal snapshot that when applied takes 'from_state' to 'to_state'.

        Removed strips and removed properties are ignored.

        The returned object can references to `from_state` and `to_state`, it doesn't create deep copies.
        """

        # Sentinel value that acts as the from-value of new properties.
        impossible_value = object()

        diff = {}
        for strip_name, to_rna_data in to_state.items():
            try:
                from_rna_data = from_state[strip_name]
            except KeyError:
                # New strip, has to be synced entirely.
                diff[strip_name] = to_rna_data
                continue

            # Existing strip, see which properties changed.
            diff_rna_data = {}
            for property_name, to_value in to_rna_data.items():
                from_value = from_rna_data.get(property_name, impossible_value)
                if from_value == to_value:
                    # Value remained the same, ignore.
                    continue
                diff_rna_data[property_name] = to_value

            if not diff_rna_data:
                continue

            # If one of the start/offset/duration/end frame properties changed, include all of them.
            remaining_properties = cls._frame_property_names - set(diff_rna_data.keys())
            for property_name in remaining_properties:
                try:
                    diff_rna_data[property_name] = to_rna_data[property_name]
                except KeyError:
                    continue

            diff[strip_name] = diff_rna_data
        return diff


class StripCreator:
    _valid_vse_strip_type_names = frozenset(
        {
            enum_item.identifier
            for enum_item in bpy.types.Sequence.bl_rna.properties["type"].enum_items
        }
    )

    def __init__(self) -> None:
        self._vse: Optional[bpy.types.SequenceEditor] = None

    def new(
        self, vse: bpy.types.SequenceEditor, strip_name: str, strip_data: RNAData
    ) -> Optional[bpy.types.Sequence]:
        """Create a new strip based on the given RNA data."""

        self._vse = vse  # so that we don't have to pass it everywhere all the time.
        try:
            strip = self._new(strip_name, strip_data)
        except KeyError as ex:
            logger.warning(
                "unable to create new strip %r, incomplete data received, missing %s (received %r)",
                strip_name,
                ex,
                strip_data,
            )
            return None
        except NotImplementedError as ex:
            logger.warning(
                "unable to create new strip %r, type %r not supported",
                strip_name,
                strip_data.get("type", "UNKNOWN"),
            )
            return None
        finally:
            self._vse = None

        logger.debug("created new strip %r", strip_name)
        return strip

    def _new(self, strip_name: str, strip_data: RNAData) -> bpy.types.Sequence:
        """Create a new strip based on the given RNA data.

        This function assumes that all the required properties for new strips
        are known and provided in `strip_data`.
        """

        strip_type = strip_data["type"]
        assert strip_type in self._valid_vse_strip_type_names

        creation_funcs = {
            "IMAGE": self._new_image,
            "MASK": self._new_mask,
            "MOVIE": self._new_movie,
            "MOVIECLIP": self._new_movieclip,
            "SCENE": self._new_scene,
            "SOUND": self._new_sound,
        }
        try:
            creator = creation_funcs[strip_type]
        except KeyError:
            # This means it's an effect strip, which has a different signature.
            return self._new_effect(strip_name, strip_type, strip_data)
        return creator(strip_name, strip_type, strip_data)

    def _new_image(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        assert self._vse

        # Construct the image strip from the first image.
        try:
            filename = strip_data["elements"][0]
        except IndexError:
            raise AttributeError("elements[0]") from None

        directory = Path(strip_data["directory"])
        strip = self._vse.sequences.new_image(
            strip_name,
            str(directory / filename),
            strip_data["channel"],
            strip_data["frame_start"],
        )

        # Append the remaining images as elements.
        for filename in strip_data["elements"][1:]:
            strip.elements.append(filename)

        return strip

    def _new_mask(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        raise NotImplementedError()

    def _new_movie(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        assert self._vse
        strip = self._vse.sequences.new_movie(
            strip_name,
            strip_data["filepath"],
            strip_data["channel"],
            strip_data["frame_start"],
        )

        # Append the remaining images as elements.
        for filename in strip_data["elements"][1:]:
            strip.elements.append(filename)

        return strip

    def _new_movieclip(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        assert self._vse
        return self._vse.sequences.new_movieclip(
            strip_name,
            strip_data["filepath"],
            strip_data["channel"],
            strip_data["frame_start"],
        )

    def _new_scene(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        raise NotImplementedError()

    def _new_sound(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        assert self._vse
        return self._vse.sequences.new_sound(
            strip_name,
            strip_data["sound"]["filepath"],
            strip_data["channel"],
            strip_data["frame_start"],
        )

    def _new_effect(
        self, strip_name: str, strip_type: str, strip_data: RNAData
    ) -> bpy.types.Sequence:
        assert self._vse
        return self._vse.sequences.new_effect(
            strip_name,
            strip_type,
            strip_data["channel"],
            strip_data["frame_start"],
            frame_end=strip_data["frame_start"] + 1,
            seq1=strip_data.get("input1"),
            seq2=strip_data.get("input2"),
            seq3=strip_data.get("input3"),
        )


def register() -> None:
    logger.debug("registering")


def unregister() -> None:
    logger.debug("unregistering")
