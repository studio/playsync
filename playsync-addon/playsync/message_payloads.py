# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The Original Code is Copyright (C) 2020 Blender Foundation.

import dataclasses
import json
import logging
import time
from typing import Any, Iterable, Dict, Optional, Set, Tuple

import bpy

# Mapping from property name to property value.
RNAData = Dict[str, Any]

# Mapping from strip name to its data.
StripSnapshot = Dict[str, RNAData]


def dict_factory_without_none(key_values: Iterable[Tuple[Any, Any]]) -> Dict[Any, Any]:
    """Factory function for dataclasses.asdict() that skips None values."""
    return dict((key, value) for key, value in key_values if value is not None)


@dataclasses.dataclass
class PlayState:
    """Payload of a C2CPlayState message."""

    playing: Optional[bool]
    frame_current: Optional[int]

    @classmethod
    def from_json_bytes(cls, message_payload: bytes) -> "PlayState":
        payload = json.loads(message_payload.decode("utf8"))
        return cls(payload.get("playing"), payload.get("frame_current"))

    def asdict(self) -> Dict[str, Any]:
        return dataclasses.asdict(self, dict_factory=dict_factory_without_none)


@dataclasses.dataclass
class DiffResult:
    removed_strip_names: Set[str]
    rna_diff: StripSnapshot
    active_strip: Optional[str]  # None = unchanged, "" = no active strip

    def __bool__(self) -> bool:
        """Return False iff the DiffResult is empty."""
        return bool(self.removed_strip_names or self.rna_diff or self.active_strip)

    @classmethod
    def from_json_bytes(cls, message_payload: bytes) -> "DiffResult":
        payload = json.loads(message_payload.decode("utf8"))
        payload.setdefault("active_strip", None)
        return cls(**payload)

    def asdict(self) -> Dict[str, Any]:
        return dataclasses.asdict(self)
